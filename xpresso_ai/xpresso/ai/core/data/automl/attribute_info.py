__all__ = ["AttributeInfo"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.logging.xpr_log import XprLogger


class AttributeInfo:

    def __init__(self, attribute_name,
                 dataset_type=DatasetType.STRUCTURED):
        self.logger = XprLogger()
        self.name = attribute_name
        self.dataset_type = dataset_type
        self.metrics = dict()
